﻿using NUnit;
using NUnit.Framework;

namespace _23_03_2020_stringcalculatorin30minutes.Test
{
    [TestFixture]
    public class TestAdd
    {
        private Calculator _calculator = null;
        [OneTimeSetUp]
        public void Initialize()
        {
            _calculator = new Calculator();
        }

        [Test]
        public void Given_EmptyString_When_Adding_Then_ReturnZero()
        {
            var expected = 0;
            var actual = _calculator.Add(string.Empty);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Given_OneNumber_When_Adding_Then_ReturnThatNumber()
        {
            var expected = 1;
            var actual = _calculator.Add("1");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Given_TwoNumbers_When_Adding_Then_ReturnSum()
        {
            var expected = 3;
            var actual = _calculator.Add("1,2");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Given_MultipleNumber_When_Adding_Then_ReturnSum()
        {
            var expected = 6;
            var actual = _calculator.Add("1,2,3");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Given_NewLine_When_Adding_Then_ReturnSum()
        {
            var expected = 6;
            var actual = _calculator.Add("1,2\n3");
        }
    }
}
