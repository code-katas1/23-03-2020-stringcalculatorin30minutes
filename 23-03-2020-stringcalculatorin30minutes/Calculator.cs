﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23_03_2020_stringcalculatorin30minutes
{
    public class Calculator
    {
        public int Add(string numbers)
        {
            if (String.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            int[] numbersArray = GetNumbers(numbers);
            return GetSum(numbersArray);
        }

        private int[] GetNumbers(string numbers)
        {
            string[] defaultDelimiters = { ",", "\n" };

            return numbers.Split(defaultDelimiters, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();
        }

        private int GetSum(int[] numbersArray)
        {
            int sum = 0;
            for (int i = 0; i < numbersArray.Length; i++)
            {
                sum += numbersArray[i];
            }

            return sum;
        }
    }
}
